```js
const [logIn, { data: logInData, loading: logInLoading, error: logInError }] =
//..

const logInRef = useRef(logIn);

useEffect(() => {
    const code = new URL(window.location.href).searchParams.get('code');
    if (code) {
      logInRef.current({
        variables: {
          input: { code },
        },
      });
    }
  }, []);

```

## Why use logInRef instead of login?

The useEffect Hook will display a warning since it'll tell us that if we want to use the logIn function, we'll need to declare it as a dependency to the Hook.

Since the logIn request is being instantiated within the component, we won't add it as a dependency to our useEffect Hook. If the component was to ever re-render, a new version of logIn will be set up which may cause our useEffect Hook to run again, which is something we'll definitely not want.

Here, we use the `useRef` Hook for the `login` mutation. The useRef Hook accepts an argument with which it returns **a mutable object which will persist for the lifetime of the component**.

```js
const logInRef = useRef(logIn);
```

In our effect callback, we can access the mutable ` logIn`` property we've passed in with the  `.current`` property of the object returned from the useRef Hook. This will look something like this:

```js
if (code) {
  // the login mutation is stored in current property
      logInRef.current({
        variables: {
          input: { code }
        }
      });
```

The `logInRef.current`` property will reference the original function regardless of how many renders happen again. Our useEffect Hook recognizes this and **doesn't require us to specify the logInRef property in the dependencies list.**

Note: The way useRef Hook is used in this example should be done sparingly. If you're dealing with actual data properties an effect may depend on, you may want to include it in the dependencies list to avoid bugs/issues. In this case however, we're referencing a function that's going to remain the same regardless of how many times our component renders!
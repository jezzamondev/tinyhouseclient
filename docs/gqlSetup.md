## CodegenConfig setup

[Apollo client docs for static-typing](https://www.apollographql.com/docs/react/development-testing/static-typing/)

```js
import { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  schema: 'http://localhost:9000/api',
  // this assumes that all your source files are in a top-level `src/` directory - you might need to adjust this to your file structure
  documents: ['src/**/*.{ts,tsx}'],
  generates: {
    './src/__generated__/': {
      preset: 'client',
      plugins: [],
      presetConfig: {
        gqlTagName: 'gql',
      },
    },
  },
  ignoreNoDocuments: true,
};

export default config;
```

Breakdown of the codegen config file for setting up graphql types on client side,

1. **`schema: 'http://localhost:9000/api',`**

   - This line specifies the location of your GraphQL schema. The code generator will fetch the schema from this URL, which is presumably the endpoint of your GraphQL server. In this case, the server is running locally on port 9000.

2. **`documents: ['src/**/\*.{ts,tsx}'],`\*\*

   - Here, `documents` refers to the GraphQL operations (queries, mutations, subscriptions) that you write in your application. This line tells the code generator where to find these operations.
   - The value `'src/**/*.{ts,tsx}'` is a glob pattern indicating that your GraphQL documents are located in TypeScript (`ts`) or TypeScript React (`tsx`) files within the `src` directory, including all of its subdirectories.

3. **`generates: { './src/__generated__/': { preset: 'client', plugins: [], presetConfig: { gqlTagName: 'gql', }, }, },`**

   - The `generates` field specifies where and how to generate the code:
     - `./src/__generated__/`: This is the directory where the generated files will be placed. It's a common practice to put generated code in a separate directory like `__generated__` to keep it distinct from hand-written code.
     - `preset: 'client'`: This indicates that the codegen should use a preset suitable for generating client-side code.
     - `plugins: []`: Here you can specify an array of plugins for code generation. It's empty in this config, meaning no additional plugins are being used.
     - `presetConfig: { gqlTagName: 'gql', }`: This is the configuration for the chosen preset. The `gqlTagName: 'gql'` setting indicates that the GraphQL tag used in your documents is `gql`. This is important for the codegen to correctly identify and process your GraphQL documents.

4. **`ignoreNoDocuments: true,`**
   - This setting tells the code generator to ignore any errors related to the absence of GraphQL documents. This can be useful if some of the files matching the `documents` glob pattern don't actually contain GraphQL queries or mutations.

In summary, this configuration sets up a GraphQL code generator that fetches the schema from a local server, searches for GraphQL documents in TypeScript and TypeScript React files inside the `src` directory, generates client-side code in a `__generated__` directory within `src`, and uses the `gql` tag to identify GraphQL documents. It's configured to ignore errors related to files without GraphQL documents.

## Using `gql` tag

In this example, if the gql template string does not match what is the generated gql function, there will be an error

```js
import { gql } from '../../../../../src/__generated__/gql';

export const LOG_OUT = gql(/* GraphQL */ `
  mutation LogOut {
    logOut {
      id
      token
      avatar
      hasWallets
      didRequest
    }
  }
`);
```

In this example the template string contains `hasWallets` with an `s` rather than `hasWallet`, when its used in the useQuery function there will be an error
`Argument of type "unknown" is not assignable to parameter of type...` which indicates the type checking worked!

Please note the addition of `/* Graphql */` helps VS Code with format highlighting.

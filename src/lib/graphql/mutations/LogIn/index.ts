import { gql } from '../../../../../src/__generated__/gql';

export const LOG_IN = gql(/* GraphQL */ `
  mutation LogIn($input: LogInInput) {
    logIn(input: $input) {
      id
      token
      avatar
      hasWallet
      didRequest
    }
  }
`);

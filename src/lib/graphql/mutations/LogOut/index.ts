import { gql } from '../../../../../src/__generated__/gql';

export const LOG_OUT = gql(/* GraphQL */ `
  mutation LogOut {
    logOut {
      id
      token
      avatar
      hasWallet
      didRequest
    }
  }
`);

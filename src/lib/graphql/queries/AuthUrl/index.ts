// import { gql } from '@apollo/client';  // deprecated
import { gql } from '../../../../../src/__generated__/gql';

export const AUTH_URL = gql(/* GraphQL */ `
  query AuthUrl {
    authUrl
  }
`);

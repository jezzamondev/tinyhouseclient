export interface Viewer {
  id?: string | null | undefined;
  token?: string | null | undefined;
  avatar?: string | null | undefined;
  hasWallet?: boolean | null | undefined;
  didRequest: boolean;
}

import { useEffect, useCallback, useReducer } from 'react';
import { server } from './server';

interface State<TData> {
  data: TData | null;
  loading: boolean;
  error: boolean;
}

interface QueryResult<TData> extends State<TData> {
  refetch: () => void;
}

type Action<TData> =
  | { type: 'FETCH' }
  | { type: 'FETCH_SUCCESS'; payload: TData }
  | { type: 'FETCH_ERROR' };

// we modified the reducer slightly so that it returns the expected function (neccessary when running)  const fetchReducer = reducer<TData>();
// this is the way to pass the Type TData to the redcucer
const reducer =
  <TData>() =>
  (state: State<TData>, action: Action<TData>): State<TData> => {
    switch (action.type) {
      case 'FETCH':
        return { ...state, loading: true };
      case 'FETCH_SUCCESS':
        return {
          ...state,
          data: action.payload,
          loading: false,
          error: false,
        };
      case 'FETCH_ERROR':
        return { ...state, loading: false, error: true };
      default:
        throw new Error();
    }
  };

// The data property within state is currently unknown because we haven't passed a value for the TData
// type variable the reducer() function expects. Here's where we can do something interesting that
// can help us achieve this. Instead of passing the reducer() function directly to the useReducer Hook,
// we can pass in a function that returns the expected reducer() function.
//  This will help us pass along the TData type variable from the useQuery Hook to the reducer() function.
export const useQuery = <TData = any>(query: string): QueryResult<TData> => {
  const fetchReducer = reducer<TData>();

  const [state, dispatch] = useReducer(fetchReducer, {
    data: null,
    loading: false,
    error: false,
  });

  const fetch = useCallback(() => {
    const fetchApi = async () => {
      try {
        dispatch({ type: 'FETCH' });
        const { data, errors } = await server.fetch<TData>({
          query,
        });

        if (errors && errors.length) {
          throw new Error(errors[0].message);
        }
        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      } catch {
        dispatch({ type: 'FETCH_ERROR' });
      }
    };

    fetchApi();
  }, [query]);

  useEffect(() => {
    fetch();
  }, [fetch]);

  return { ...state, refetch: fetch };
};

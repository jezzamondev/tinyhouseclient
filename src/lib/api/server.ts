interface Body<TVariables> {
  query: string;
  variables?: TVariables;
}

interface Error {
  message: string;
}

export const server = {
  fetch: async <TData = any, TVariables = any>(body: Body<TVariables>) => {
    // In our server.fetch() function, we can specify an argument of /api in the window fetch() method. /api will get
    // proxied to http://localhost:9000/api (see package.json)
    const res = await fetch('/api', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
    // The window fetch() function provides an ok property within the response which can be used to determine if the response was successful (i.e. the response status is in the range 200 - 299).
    if (!res.ok) {
      throw new Error('failed to fetch from server');
    }
    // type assert here since we know to expect a TData generic
    // graphql will return a success 200 even if there was an error, it will pass it to the property errors
    return res.json() as Promise<{ data: TData; errors: Error[] }>;
  },
};

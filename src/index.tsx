import React, { useEffect, useRef, useState } from 'react';
import { render } from 'react-dom';
import { ApolloClient, ApolloLink, HttpLink, concat } from '@apollo/client';
import {
  ApolloProvider,
  InMemoryCache,
  useMutation,
} from '@apollo/react-hooks';
import { LOG_IN } from './lib/graphql/mutations/LogIn';
import {
  LogInMutation as LogInData,
  LogInMutationVariables as LogInVariables,
} from './__generated__/graphql';
import { Viewer } from './lib/types';

import { Layout, Spin, Affix } from 'antd';
import { AppHeaderSkeleton } from './lib/components';

import {
  AppHeader,
  Home,
  Host,
  Listing,
  Listings,
  Login,
  NotFound,
  User,
} from './sections';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './styles/index.css';
import reportWebVitals from './reportWebVitals';

// https://www.apollographql.com/docs/react/networking/advanced-http-networking
/* *********************************************** */
const httpLink = new HttpLink({ uri: '/api' });

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      'X-CSRF-TOKEN': sessionStorage.getItem('token') || '',
    },
  }));

  return forward(operation);
});

const client = new ApolloClient({
  // uri: '/api',  // remove default for httpLink custom

  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink),
});

/* *********************************************** */

const initialViewer: Viewer = {
  id: null,
  token: null,
  avatar: null,
  hasWallet: null,
  didRequest: false,
};

const App = () => {
  const [viewer, setViewer] = useState<Viewer>(initialViewer);
  // in this context we are logging in with cookie if existing on loading the APP (ie previously logged in)
  const [logIn, { error }] = useMutation<LogInData, LogInVariables>(LOG_IN, {
    onCompleted: (data) => {
      if (data && data.logIn) {
        setViewer(data.logIn);

        if (data.logIn.token) {
          sessionStorage.setItem('token', data.logIn.token);
        } else {
          sessionStorage.removeItem('token');
        }
      }
    },
  });

  const logInRef = useRef(logIn);

  useEffect(() => {
    logInRef.current();
  }, []);

  if (!viewer.didRequest && !error) {
    return (
      <Layout className="app-skeleton">
        <AppHeaderSkeleton />
        <div className="app-skeleton__spin-section">
          <Spin size="large" tip="Launching JezzyHouse" />
        </div>
      </Layout>
    );
  }

  return (
    <Router>
      <Layout id="app">
        <Affix offsetTop={0} className="app__affix-header">
          <AppHeader viewer={viewer} setViewer={setViewer} />
        </Affix>
        <Switch>
          <Route exact path="/" component={Home} />
          {/* <Route exact path="/login" component={Login} /> */}
          <Route
            exact
            path="/login"
            render={(props) => <Login {...props} setViewer={setViewer} />}
          />
          <Route exact path="/host" component={Host} />
          <Route exact path="/listing/:id" component={Listing} />
          <Route exact path="/listings/:location?" component={Listings} />
          <Route exact path="/user/:id" component={User} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </Router>
  );
};

render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
